/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImgBlockClass;

import java.awt.Color;
import java.awt.image.*;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author johne
 */
public class LineChart extends javax.swing.JFrame{
//ApplicationFrame {

    /**
     * Creates a Line Chart.
     *
     * @param LineChart  the frame title.
     */
    
    JFreeChart chart;
    public LineChart(double[][] GlTf) {

        //super(title);

        final XYDataset dataset = createDataset(GlTf);
        //final JFreeChart chart = createChart(dataset);
        chart = createChart(dataset);
    }
    
    /**
     * Creates a sample dataset.
     * 
     * @return a sample dataset.
     */
    private XYDataset createDataset(double[][] GlTf) {
        
        final XYSeries series1 = new XYSeries("GlTf");
        for (int i=0; i < 255; i++){
            series1.add(GlTf[0][i], GlTf[1][i]);
        }
        
        final XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(series1);
        //dataset.addSeries(series2);
        //dataset.addSeries(series3); 
        return dataset;
        
    }
    
    public BufferedImage convertChar2Img(int w, int h) {
        // Convert the JFree chart to an image
        BufferedImage objBufferedImage = chart.createBufferedImage(h, w);
        return objBufferedImage;
        }

    
    
    /**
     * Creates a chart.
     * 
     * @param dataset  the data for the chart.
     * 
     * @return a chart.
     */
    private JFreeChart createChart(final XYDataset dataset) {
        
        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
            null,           // chart title
            null,                      // x axis label
            null,                      // y axis label
            dataset,                  // data
            PlotOrientation.VERTICAL,
            false,                     // include legend
            false,                     // tooltips
            false                     // urls
        );                   // urls

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(Color.white);

//        final StandardLegend legend = (StandardLegend) chart.getLegend();
  //      legend.setDisplaySeriesShapes(true);
        
        // get a reference to the plot for further customisation...
        final XYPlot xyPlot = chart.getXYPlot();
        xyPlot.setBackgroundPaint(Color.lightGray);
    //    plot.setAxisOffset(new Spacer(Spacer.ABSOLUTE, 5.0, 5.0, 5.0, 5.0));
        xyPlot.setDomainGridlinePaint(Color.white);
        xyPlot.setRangeGridlinePaint(Color.white);
        
        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(1, false);
        renderer.setSeriesShapesVisible(0, false);
        xyPlot.setRenderer(renderer);
        // change the auto tick unit selection to integer units only...
        // final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        //rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        
        NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
        domain.setRange(0.0, 1.0);
        //domain.setTickUnit(new NumberTickUnit(0.1));
        //domain.setVerticalTickLabels(true);
        NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
        range.setRange(0.0, 1.0);
        //range.setTickUnit(new NumberTickUnit(0.1));
        
        
        // OPTIONAL CUSTOMISATION COMPLETED.
                
        return chart;
        
    }
    
}
