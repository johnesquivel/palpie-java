/*
 * IMGBLOCK CLASS
 * This class is contains the image data and functions for log and power
 * transforms.
 * An image block must be initialized with 3 swing labels for the input, ouput,
 * and tranfer curve image boxes.
 * 
 * Public methods:
 *      addInputFile
 */
package ImgBlockClass;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

import static java.lang.Math.pow;
import static java.lang.Math.log;
import static java.lang.Math.sqrt;
import static java.lang.Math.min;
import static java.lang.Math.max;


/**
 *
 * @author johne
 */
public class ImgBlock {
    
    
    // Class Attributes
    public JLabel inLabel, curLabel, outLabel;
    private double[][][] inData, outData; // Input & output data
    private double[][] GlTf;  // gray level transfer curve row0 = x, row1 = y
    private int inW, inH, curW, curH, outW, outH; // width and height of pic labels
    private int dataW, dataH; // width and height of image data
    private String funcType;
    private double expValue;
    private int xl, xr, yt, yb; // 4 corners of the region selection box
    private long nOps;  // number of operation, make long since it could get big

    
    // CONSTRUCTOR
    public ImgBlock(JLabel inLabel, JLabel curLabel, JLabel outLabel) {
        this.inLabel = inLabel;
        this.curLabel = curLabel;
        this.outLabel = outLabel;
        this.inW = inLabel.getWidth();
        this.inH = inLabel.getHeight();
        this.curW = curLabel.getWidth();
        this.curH = curLabel.getHeight();
        this.outW = outLabel.getWidth();
        this.outH = outLabel.getHeight();
        initComponents();
    }
    
    // Initialize the tranfer curve.  These values will be over written when
    // the imgManip function is called.
    private void initComponents() {
        // Until we load a file, make the data size the same as the img size
        dataW = inW;
        dataH = inH;
        // Set default region box
        xl = 0;
        xr = dataW;
        yt = 0;
        yb = dataH;
        
        // Create default original data
        inData = new double[inW][inH][3];
        outData = new double[inW][inH][3];
        inData = paintBlack(inData);
        outData = paintBlack(outData);
        
        // Set default values by running initial imgManip call.
        imgManip("log", 2.0);  // Therefore log, 2.0 are the default values
        
        // Initialize number of operations to 0
        nOps = 0;
    }
      
    
    // This function is the top level (external) image manipulation function
    // that applies either log or power function with the exp value to the inData.
    public void imgManip(String funcType, double expValue) {
        // Update values in this object with new values.
        this.funcType = funcType;
        this.expValue = expValue;
        
        // Make sure output data size is same as input
        copyData();

        // Build new tranfer curve
        GlTf = calcTC();
        
        // Run tranfer function on inData
        applyTF();
        
        // Update all image on the GUI
        updatePicLabels();
    }
    
    
    // This function opens the input file and applys the imgManip method to it.
    public void addInputFile(File inputFile) {
        // Initialization
        BufferedImage img;
        
        // Try opening the input image from the file name
        try {
            img = ImageIO.read(inputFile);
            // Get the size of the image
            dataW = img.getWidth();
            dataH = img.getHeight();
            // If we got a good file, then convert, apply tr, and update images
            inData = convertImg2Data(img);
            resetRegionSelection();
            } catch (IOException e){
                ErrorBox.infoBox("The image file you tried to load does not exist!", "File Doesn't Exist");
            }
        
    }
    
    // Save the image to the output file
    public void saveOutputFile(File outputFile) {
        try {
                // retrieve image
                BufferedImage outImg = convertData2Img(outData);
                // write the image
                ImageIO.write(outImg, "png", outputFile);
                
            } catch (IOException e) {
                ErrorBox.infoBox("Was not able to save the file!", "Can't Save File");
            }
    }
            
    // Convert the buffered image into a 3d matrix (x,y,rgb channel)
    private double[][][] convertImg2Data(BufferedImage img) {
        // Initialize
        int[][] arr = new int[dataW][dataH];
        double[][][] imgData = new double[dataW][dataH][3];
        // First convert to int from image
        for(int ix = 0; ix < dataW; ix++)
            for(int iy = 0; iy < dataH; iy++)
                arr[ix][iy] = (int)img.getRGB(ix, iy);
        // Then convert int to double with seperate channel for each color
        for (int ix=0; ix < dataW; ix++) {
            for (int iy=0; iy < dataH; iy++) {
                // Extract blue int and convert to double
                imgData[ix][iy][0] = (double)((arr[ix][iy] & 0xff));
                // Extract green int and convert to double
                imgData[ix][iy][1] = (double)((arr[ix][iy] & 0xff00) >> 8);
                // Extract red int and convert to double
                imgData[ix][iy][2] = (double)((arr[ix][iy] & 0xff0000) >> 16);
                // Normalize to 255
                imgData[ix][iy][0] /= 255.0;
                imgData[ix][iy][1] /= 255.0;
                imgData[ix][iy][2] /= 255.0;
            }
        }
        return imgData;
    } 
    
    // Convert the image matrix back into a buffered image.
    private BufferedImage convertData2Img(double[][][] data) {
        // Initialization
        BufferedImage img = new BufferedImage(dataW, dataH, BufferedImage.TYPE_INT_RGB);
        int[][][] arr = new int[dataW][dataH][3];
        int[][] argb = new int[dataW][dataH];
        
        // First convert from normalized double to grayscale int
        for (int ix=0; ix < dataW; ix++)
            for (int iy=0; iy < dataH; iy++)
                for (int ic=0; ic < 3; ic++)
                    arr[ix][iy][ic] = (int)(data[ix][iy][ic] * 255);
        
        // Now combine all channels into one value for ARGB
        for (int ix=0; ix < dataW; ix++) {
            for (int iy=0; iy < dataH; iy++) {
                argb[ix][iy] = (arr[ix][iy][0] & 0xff); // blue
                argb[ix][iy] += ((arr[ix][iy][1] & 0xff) << 8); // green
                argb[ix][iy] += ((arr[ix][iy][2] & 0xff) << 16); // red
                //argb[ix][iy] += 0x00000000; //alpha
                }
            }
        
        // Final cast argb back into an img
        for (int ix=0; ix < dataW; ix++)
            for (int iy=0; iy < dataH; iy++)
                img.setRGB(ix, iy, argb[ix][iy]);

        return img;
    }

       
    // Update the images on the GUI with new labels images
    private void updatePicLabels() {
        // Convert input & output data back into image
        BufferedImage inImg = convertData2Img(inData);
        BufferedImage outImg = convertData2Img(outData);
        
        // Scale the input image then post to input label
        Image scaledIn = inImg.getScaledInstance(inW, inH, Image.SCALE_SMOOTH);
        inLabel.setIcon(new javax.swing.ImageIcon(scaledIn));
        
        // Scale the output image then post to output label
        Image scaledOut = outImg.getScaledInstance(outW, outH, Image.SCALE_SMOOTH);
        outLabel.setIcon(new javax.swing.ImageIcon(scaledOut));
        
        // Build tranfer curve chart, then scale, then post to curve label
        final LineChart trCur = new LineChart(GlTf);
        BufferedImage curImg = trCur.convertChar2Img(176, 296);
        Image scaledCurve = curImg.getScaledInstance(curW, curH, Image.SCALE_SMOOTH);
        curLabel.setIcon(new javax.swing.ImageIcon(scaledCurve));
    }
               
    
    // Calculate the data for the tranfer curve that will be passed to the chart 
    private double[][] calcTC() {
        double[][] newGL = new double[2][255];  // New graylevel
        // Sweep through 255 graylevels and calculate the new tranfer curve's
        // y data.
        if ( funcType.equalsIgnoreCase("power") ) {
            for (int i=0; i < 255; i++) {
                newGL[0][i] = i/255.0;  // The .0 makes it double
                newGL[1][i] = max(0.0, min(1.0, pow((i/255.0), expValue )));
            }
        }
        else if ( funcType.equalsIgnoreCase("log") ) {
            for (int i=0; i < 255; i++) {
                // log with different base doesn't exist to using natural log
                // Note that for log the value must have a 1.0 offset
                newGL[0][i] = i/255.0;  // The .0 makes it double
                newGL[1][i] = max(0.0, min(1.0, log((i/255.0) + 1.0 ) / log(expValue) ));
            }            
        }
        return newGL;
    }
    
    
    // Apply the region selection box to the image with the function type
    // and exponent that are already loaded.
    public void regionSelection(int xStart, int yStart, int xEnd, int yEnd) {
        // set the region box and run img manipulation
        double xRatio = (double)dataW / (double)inW;
        double yRatio = (double)dataH / (double)inH;
        xl = (int)( (double)xStart * xRatio );
        xr = (int)( (double)xEnd * xRatio );
        yt = (int)( (double)yStart * yRatio );
        yb = (int)( (double)yEnd * yRatio );
        imgManip(funcType, expValue);        
    }
    
    // Reset the region selection box back to the whole image and rerun the
    // image manipulation method
    public void resetRegionSelection() {
        // reset the region box and run img manipulation
        xl = 0;
        xr = dataW;
        yt = 0;
        yb = dataH;
        imgManip(funcType, expValue);
    }
    
    // Calculate the mean of the data, where the 1st dimesion is orig/new and
    // 2nd dimension is BGR value.  For instance avg[0][2] is original red mean.
    // If norm is true then result is normalized.
    public double[][] get_mean(boolean norm) {
        // Calculate the mean of the input data
        double nc;
        double N = 0;
        double[][] avg = {{0.0, 0.0, 0.0},{0.0, 0.0, 0.0}};
        //for (int ix=0; ix < dataW; ix++) {
            //for (int iy=0; iy < dataH; iy++) {
        for (int ix=xl; ix < xr; ix++) {
            for (int iy=yt; iy < yb; iy++) {
                for (int ic=0; ic < 3; ic++) {
                    avg[0][ic] += inData[ix][iy][ic];
                    avg[1][ic] += outData[ix][iy][ic];
                }
                N++; // Count the number of pixels to divide by for average
            }
        }
        // Set normalization coeffiecient nc, so that if normilized is true then
        // nc=1 if not then nc = 255
        if (norm) {
            nc = 1.0;
        }
        else {
            nc = 255.0;
        }
        
        // Divide the sum by N for average and apply normilization coeficient
        for (int ic=0; ic < 3; ic++) {
            avg[0][ic] = (avg[0][ic] / N) * nc;
            avg[1][ic] = (avg[1][ic] / N) * nc;
        }
        return avg;
    }

    
    // Calculate the std of the data, where the 1st dimesion is orig/new and
    // 2nd dimension is BGR value.  For instance std[0][2] is original red std.
    // If norm is true the result is normalized.
    public double[][] get_std() {
        // First step is to get the mean
        double[][] M = get_mean(true);
        
        // Then iterate through the data to get the variance.
        double N = 0;
        double[][] std = {{0.0, 0.0, 0.0},{0.0, 0.0, 0.0}};
        //for (int ix=0; ix < dataW; ix++) {
            //for (int iy=0; iy < dataH; iy++) {
        for (int ix=xl; ix < xr; ix++) {
            for (int iy=yt; iy < yb; iy++) {
                for (int ic=0; ic < 3; ic++) {
                    std[0][ic] += pow((inData[ix][iy][ic] - M[0][ic]), 2);
                    std[1][ic] += pow((outData[ix][iy][ic] - M[1][ic]), 2);
                }
                N++; // Count the number of pixels to divide by for average
            }
        }
        
        // Finally divide by N and take sqrt of variance to get std dev.
        for (int ic=0; ic < 3; ic++) {
            std[0][ic] = sqrt(std[0][ic] / N);
            std[1][ic] = sqrt(std[1][ic] / N);
        }
        return std;
    }
    
    // Accessor to number of operations done during the last image manipulation
    public long get_nOps() {
        return nOps;
    }
    
    
    // Copy all data from inData to outData
    private void copyData() {
        // Initialize
        outData = new double[dataW][dataH][3];
        // Copy all elements
        for (int ix=0; ix < dataW; ix++) {
            for (int iy=0; iy < dataH; iy++) {
                System.arraycopy(inData[ix][iy], 0, outData[ix][iy], 0, 3);
            }
        }
    }
        
    
    // Apply the transform; tf(inData) = outData    
    private void applyTF() {
        // Start by coping the data from input to output that way if region is
        //   selected then everything outside the box is the same.
        copyData();
        // Initialize the number of operations performed to 0
        nOps = 0;
        if (funcType.compareToIgnoreCase("log") == 0) {
            for (int ix=xl; ix < xr; ix++) {
                for (int iy=yt; iy < yb; iy++) {
                    for (int ic=0; ic < 3; ic++) {
                        outData[ix][iy][ic]
                                = log(inData[ix][iy][ic] + 1.0) / log(expValue);
                        outData[ix][iy][ic] = max(0.0, min(1.0, outData[ix][iy][ic]));
                        nOps++;
                    }
                }
            }
        }
        else {
            for (int ix=xl; ix < xr; ix++) {
                for (int iy=yt; iy < yb; iy++) {
                    for (int ic=0; ic < 3; ic++) {
                        outData[ix][iy][ic]
                                = pow(inData[ix][iy][ic], expValue);
                        outData[ix][iy][ic] = max(0.0, min(1.0, outData[ix][iy][ic] ));
                        nOps++;
                    }
                }
            }
        }
    }
    

    // Set all data = 0.0 (black)
    private double[][][] paintBlack(double[][][] data) {
        for (int ix=0; ix < dataW; ix++)
            for (int iy=0; iy < dataH; iy++)
                for (int ic=0; ic < 3; ic++)
                    data[ix][iy][ic] = 0.0;
        return data;
    }
   
}
