# README #

PALPIE - Power and Logarithm Photo Image Enhancement

### What is this repository for? ###

* The is a simple java GUI example which can apply a log or power function to all pixels in an image.
* Version: 1.0

### How to install ###

*  You can either download the source by cloning this repo by running

```
git clone https://bitbucket.org/johnesquivel/palpie-java.git
```

*  Or you can download the compiled java version in the download section.